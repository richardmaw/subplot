use std::collections::HashSet;

use pandoc_ast::{Block, MutVisitor};

#[derive(Default)]
pub struct BlockClassVisitor {
    pub classes: HashSet<String>,
}

impl MutVisitor for BlockClassVisitor {
    fn visit_vec_block(&mut self, vec_block: &mut Vec<Block>) {
        for block in vec_block {
            match block {
                Block::CodeBlock(attr, _) => {
                    for class in &attr.1 {
                        self.classes.insert(class.to_string());
                    }
                }
                _ => {
                    self.visit_block(block);
                }
            }
        }
    }
}
